# Example Gitlab Pipeline for a Google Cloud Function

### Requirements:
- VPC
    - with Google Private Access
- GCS bucket
- Service Account with roles:
    - roles/storage.admin
    - roles/vpcaccess.admin
    - roles/iam.serviceAccountUser
    - roles/cloudfunctions.admin
