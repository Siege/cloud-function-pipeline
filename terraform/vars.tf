variable "project" {
    type = string
}

variable "region" {
    type = string
}

variable "zone" {
    type = string
}

variable "vpc" {
    type = string
}

variable "service_account"{
    type = string
}

variable "gcs_bucket"{
    type = string
    default = "cj-storage"
}