
terraform {
    backend "gcs" {
        bucket = "cj-storage"
        prefix = "terraform/state"
    }
}

provider "google" {
    project = var.project
    region  = var.region
    zone    = var.zone
}

resource "google_vpc_access_connector" "connector" {
  name          = "cj-vpc-con"
  ip_cidr_range = "10.8.0.0/28"
  network       = var.vpc
}

data "archive_file" "test_function_zip"{
    type    = "zip"
    source_dir = "../functions"
    output_path = "./tmp/test_function.zip"
}

resource "google_storage_bucket_object" "test_function_archive" {
  name   = "test_function.zip"
  bucket = var.gcs_bucket
  source = "./tmp/test_function.zip"
  depends_on = [data.archive_file.test_function_zip]
}

resource "google_cloudfunctions_function" "test_function" {
    name                            = "test-function"
    entry_point                     = "testFunction"
    available_memory_mb             = 128
    timeout                         = 61
    project                         = var.project
    region                          = var.region
    trigger_http                    = true
    source_archive_bucket           = var.gcs_bucket
    source_archive_object           = google_storage_bucket_object.test_function_archive.name
    runtime                         = "nodejs10"
    vpc_connector                   = google_vpc_access_connector.connector.id
    vpc_connector_egress_settings   = "ALL_TRAFFIC"
    ingress_settings                = "ALLOW_INTERNAL_ONLY"
}


data "google_iam_policy" "admin" {
  binding {
    role = "roles/cloudfunctions.invoker"
    members = [
      "serviceAccount:${var.service_account}",
    ]
  }
}

resource "google_cloudfunctions_function_iam_policy" "policy" {
  project = google_cloudfunctions_function.test_function.project
  region = google_cloudfunctions_function.test_function.region
  cloud_function = google_cloudfunctions_function.test_function.name
  policy_data = data.google_iam_policy.admin.policy_data
}