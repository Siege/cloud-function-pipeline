#!/bin/bash

# Alias Helpers
alias tf="terraform"
alias tfv="terraform validate"
alias tfi="terraform init"
alias tfp="terraform plan -out tfout"
alias tfa="terraform apply tfout"